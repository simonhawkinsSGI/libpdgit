//
//  libPD.m
//  libPd cordova/phonegap plug-in implementation file
//
//  Created by Sahil Alaracu on 15/06/2015
//  USING PROJECT AS BASE: https://github.com/alesaccoia/phonegap-libpd
//

#import "libPDSGI.h"
#import "PdAudioController.h"


@implementation libPDReceiver
#pragma mark - PdRecieverDelegate

- (void)receivePrint:(NSString *)message {
    NSLog(@"%@", message);
}

- (void)receiveBangFromSource:(NSString *)source {
    NSLog(@"Bang from %@", source);
}

- (void)receiveFloat:(float)received fromSource:(NSString *)source {
    NSLog(@"Float from %@: %f", source, received);
}

- (void)receiveSymbol:(NSString *)symbol fromSource:(NSString *)source {
    NSLog(@"Symbol from %@: %@", source, symbol);
}

- (void)receiveList:(NSArray *)list fromSource:(NSString *)source {
    NSLog(@"List from %@", source);
}

- (void)receiveMessage:(NSString *)message withArguments:(NSArray *)arguments fromSource:(NSString *)source {
    NSLog(@"Message to %@ from %@", message, source);
}

- (void)receiveNoteOn:(int)pitch withVelocity:(int)velocity forChannel:(int)channel{
    NSLog(@"NoteOn: %d %d %d", channel, pitch, velocity);
}

- (void)receiveControlChange:(int)value forController:(int)controller forChannel:(int)channel{
    NSLog(@"Control Change: %d %d %d", channel, controller, value);
}

- (void)receiveProgramChange:(int)value forChannel:(int)channel{
    NSLog(@"Program Change: %d %d", channel, value);
}

- (void)receivePitchBend:(int)value forChannel:(int)channel{
    NSLog(@"Pitch Bend: %d %d", channel, value);
}

- (void)receiveAftertouch:(int)value forChannel:(int)channel{
    NSLog(@"Aftertouch: %d %d", channel, value);
}

- (void)receivePolyAftertouch:(int)value forPitch:(int)pitch forChannel:(int)channel{
    NSLog(@"Poly Aftertouch: %d %d %d", channel, pitch, value);
}

- (void)receiveMidiByte:(int)byte forPort:(int)port{
    NSLog(@"Midi Byte: %d 0x%X", port, byte);
}
@end


#pragma mark - pdLib plug-in implementation

@interface Libpd () {}

@property (nonatomic, retain) PdAudioController *audioController;
@property (nonatomic, retain) libPDReceiver *receiver;

- (BOOL)setupPd;

@end


@implementation Libpd
@synthesize audioController = audioController_;
@synthesize receiver = receiver_;
@synthesize patch = patch_;

#pragma mark - public plug-in methods

- (void)init:(CDVInvokedUrlCommand*)command {
    self.receiver = [[libPDReceiver alloc] init];
    
    [self.commandDelegate runInBackground:^{
        BOOL retValue = [self setupPd];
        CDVPluginResult* pluginResult = nil;
        if (retValue) {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"success"];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"error"];
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

- (void)deinit:(CDVInvokedUrlCommand*)command {
    [PdBase computeAudio:NO];
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"success"] callbackId:command.callbackId];
}

- (void)openPatch:(CDVInvokedUrlCommand*)command {
    [self.commandDelegate runInBackground:^{
        CDVPluginResult* pluginResult = nil;

        if([self isHeadsetEnabled]){
            NSLog(@"Audio jack plugged in");
            
            NSString *patchName = [command.arguments objectAtIndex:0];
            self.patch = [PdFile openFileNamed:patchName path:[NSString stringWithFormat:@"%@/www/", [[NSBundle mainBundle] bundlePath]]];
            NSLog(@"%@", self.patch);
            if (self.patch) {
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"success"];
            } else {
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"error"];
            }
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"HEADPHONES_NOT_PLUGGED_IN"];
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
    
}

- (void)closePatch:(CDVInvokedUrlCommand*)command {
    [self.patch closeFile];
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK] callbackId:command.callbackId];
}

- (void)addPath:(CDVInvokedUrlCommand*)command {
    NSDictionary *args = command.arguments[0];
    NSString *thePath = [args objectForKey:@"path"];
    NSString *pathWithWildcard = @"%@";
    pathWithWildcard = [pathWithWildcard stringByAppendingString:thePath];
    [PdBase addToSearchPath:[NSString stringWithFormat:pathWithWildcard, [[NSBundle mainBundle] bundlePath]]];
    [self.commandDelegate sendPluginResult:[CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"success"] callbackId:command.callbackId];
}

// Interaction with the patch

- (void)sendBang:(CDVInvokedUrlCommand*)command {
    NSDictionary *args = command.arguments[0];
    NSString *receiver = [args objectForKey:@"receiver"];
    [PdBase sendBangToReceiver:receiver];
    CDVPluginResult* pluginResult = nil;
    if (receiver != nil) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"success"];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Arg was null"];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

- (void)sendFloat:(CDVInvokedUrlCommand*)command {
    NSDictionary *args = command.arguments[0];
    NSString *receiver = [args objectForKey:@"receiver"];
    float number = [[args objectForKey:@"num"] floatValue];
    [PdBase sendFloat:number toReceiver:receiver];
    
    CDVPluginResult* pluginResult = nil;
    if (receiver != nil) {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"success"];
    } else {
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString:@"Arg was null"];
    }
    [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
}

#pragma mark - pdLib initialization

- (BOOL)setupPd {
    self.audioController = [[PdAudioController alloc] init];
    PdAudioStatus status = [self.audioController configurePlaybackWithSampleRate:44100
                                                                  numberChannels:2
                                                                    inputEnabled:YES
                                                                   mixingEnabled:YES];
    if (status == PdAudioError) {
        NSLog(@"Error! Could not configure PdAudioController");
        return FALSE;
    } else if (status == PdAudioPropertyChanged) {
        NSLog(@"Warning: some of the audio parameters were not accceptable.");
        return FALSE;
    } else {
        NSLog(@"Audio Configuration successful.");
    }
    self.audioController.active = YES;
    
    // log actual settings
    [self.audioController print];
    
    // set AppDelegate as PdRecieverDelegate to receive messages from pd
    [PdBase setDelegate:self.receiver];
    [PdBase setMidiDelegate:self.receiver]; // for midi too
    // recieve messages to fromPD: [r fromPD]
    [PdBase subscribe:@"fromPD"];
    
    // enable audio
    [PdBase computeAudio:YES];
    
    return TRUE;
}

- (BOOL) isHeadsetEnabled
{
    
    // initialise the audio session - this should only be done once - so move this line to your AppDelegate
    AudioSessionInitialize(NULL, NULL, NULL, NULL);
    UInt32 routeSize;
    
    // oddly, without calling this method caused an error.
    AudioSessionGetPropertySize(kAudioSessionProperty_AudioRouteDescription, &routeSize);
    CFDictionaryRef desc; // this is the dictionary to contain descriptions
    
    // make the call to get the audio description and populate the desc dictionary
    AudioSessionGetProperty (kAudioSessionProperty_AudioRouteDescription, &routeSize, &desc);
    
    // the dictionary contains 2 keys, for input and output. Get output array
    CFArrayRef outputs = CFDictionaryGetValue(desc, kAudioSession_AudioRouteKey_Outputs);
    
    // the output array contains 1 element - a dictionary
    CFDictionaryRef dict = CFArrayGetValueAtIndex(outputs, 0);
    
    // get the output description from the dictionary
    CFStringRef output = CFDictionaryGetValue(dict, kAudioSession_AudioRouteKey_Type);
    
    /**
     These are the possible output types:
     kAudioSessionOutputRoute_LineOut
     kAudioSessionOutputRoute_Headphones
     kAudioSessionOutputRoute_BluetoothHFP
     kAudioSessionOutputRoute_BluetoothA2DP
     kAudioSessionOutputRoute_BuiltInReceiver
     kAudioSessionOutputRoute_BuiltInSpeaker
     kAudioSessionOutputRoute_USBAudio
     kAudioSessionOutputRoute_HDMI
     kAudioSessionOutputRoute_AirPlay
     */
    
    return CFStringCompare(output, kAudioSessionOutputRoute_Headphones, 0) == kCFCompareEqualTo;
}


@end
