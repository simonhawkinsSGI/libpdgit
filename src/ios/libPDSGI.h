//
//  libPD.h
//  phonegap-libPd
//
//  Created by Sahil Alaracu on 15/06/2015//
// 	USING PROJECT AS BASE: https://github.com/alesaccoia/phonegap-libpd
//

#import <Cordova/CDVPlugin.h>
#import "PdBase.h"
#import "PdFile.h"

@interface libPDReceiver : NSObject <PdReceiverDelegate, PdMidiReceiverDelegate>

@end

@interface Libpd : CDVPlugin
- (void)init:(CDVInvokedUrlCommand*)command;
- (void)deinit:(CDVInvokedUrlCommand*)command;
- (void)openPatch:(CDVInvokedUrlCommand*)command;
- (void)closePatch:(CDVInvokedUrlCommand*)command;
- (void)addPath:(CDVInvokedUrlCommand*)command;
- (void)sendBang:(CDVInvokedUrlCommand*)command;
- (void)sendFloat:(CDVInvokedUrlCommand*)command;
- (BOOL) isHeadsetEnabled;

@property (nonatomic, retain) PdFile *patch;
@end