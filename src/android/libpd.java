package com.sgil.libpd;

import android.content.Context;
import android.media.AudioManager;
import android.util.Log;
import android.widget.Toast;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaInterface;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaWebView;
import org.apache.cordova.PluginResult;
import org.json.JSONArray;
import org.json.JSONException;
import org.puredata.android.io.AudioParameters;
import org.puredata.android.io.PdAudio;
import org.puredata.android.utils.PdUiDispatcher;
import org.puredata.core.PdBase;
import org.puredata.core.utils.IoUtils;

import java.io.File;
import java.io.IOException;


public class Libpd extends CordovaPlugin {
    private static final String TAG = "libpd_plugin";
    private Exception exception;
    private Context AppContext;
    private PdUiDispatcher dispatcher;
    private static final int MIN_SAMPLE_RATE = 44100;
    private CallbackContext thisCallbackContext;
    private CordovaInterface cordovaPlugin;

    public Libpd() { // constructor
    }

    private Context getApplicationContext() {
        return this.cordova.getActivity().getApplicationContext();
    }

    public void initialize(CordovaInterface cordova, CordovaWebView webView) {
        super.initialize(cordova, webView);
        Log.v(TAG, "Init LIBPD PLUGIN");
    }

    public boolean execute(final String action, final JSONArray args,
                           final CallbackContext callbackContext) throws JSONException {

        if (action.equals("init")) { //check if headphone is connected
            this.cordovaPlugin = cordova;
            this.initPd();
        } else if (!this.isHeadsetEnabled()) {
            callbackContext.error("HEADPHONES_NOT_PLUGGED_IN");
            return false;
        } else {

            cordova.getThreadPool().execute(new Runnable() {

                // @SuppressLint("NewApi")
                public void run() {

                    String passedArgument1 = null;
                    String passedArgument2 = null;
                    try {
                        passedArgument1 = (String) args.get(0).toString();
                        passedArgument2 = (String) args.get(1).toString();
                    } catch (JSONException e1) {
                    }

                    //Log.v(TAG, "Plugin received:" + action);
                    Libpd libpd = new Libpd();
                    libpd.cordovaPlugin = cordova;
                    libpd.AppContext = getApplicationContext();
                    libpd.thisCallbackContext = callbackContext;

                    if (action.equals("init")) {
                        libpd.initPd();
                    } else if (action.equals("openPatch")) {
                        //libpd.play(libpd.AppContext);
                        try {
                            String libpdPatchName = passedArgument2;
                            libpd.loadPatch(libpd.AppContext, libpdPatchName);
                        } catch (IOException e) {
                            Log.e(TAG, e.toString());
                        }

                    } else if (action.equals("deinit")) {
                        libpd.deinit();
                    } else if (action.equals("addPath")) {
                        libpd.addPath(passedArgument1);
                    } else if (action.equals("closePatch")) {
                        libpd.closePatch();
                    } else if (action.equals("sendBang")) {
                        libpd.sendBang(passedArgument1);
                    } else if (action.equals("sendFloat")) {
                        libpd.sendFloat(passedArgument1, passedArgument2);
                    }
                }

            });
        }

        return true;
    }

    private void initPd() {
        try {
            int sampleRate = AudioParameters.suggestSampleRate();
            int inpch = AudioParameters.suggestInputChannels();
            int outpch = AudioParameters.suggestOutputChannels();

            PdAudio.initAudio(sampleRate, inpch, outpch, 8, false);

            // Create and install the dispatcher
            this.cordovaPlugin.getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    // Create and install the dispatcher
                    dispatcher = new PdUiDispatcher();
                    PdBase.setReceiver(dispatcher);
                }
            });
        } catch (IOException e) {
            Log.e(TAG, e.toString());
            // libpd.finish();
        }

    }

    private void loadPatch(Context ctx, String libpdPatchPath) throws IOException {
        try {
            File dir = ctx.getFilesDir();
            //File dir = new File("/sdcard/mypatches");
            IoUtils.extractZipResource(
                    ctx.getResources().openRawResource(R.raw.al_quran_final_2), dir, false);
            File patchFile = new File(dir, "Al_Quran.pd");
            PdBase.openPatch(patchFile.getAbsolutePath());
            PdAudio.startAudio(ctx);
            this.thisCallbackContext.success("success");
        } catch (IOException e) {
            Log.e(TAG, e.toString());
            this.thisCallbackContext.error("error: " + e.toString());

        }
    }

    protected void deinit() {
        try {
            PdAudio.stopAudio();
            PdAudio.release();
            PdBase.release();
            this.initPd(); //initialise pd again otherwise opening the patch again won't start the audio
            this.thisCallbackContext.success("success");
        } catch (Exception e) {
            this.thisCallbackContext.error("Error: " + e.toString());
        }
    }

    public void addPath(String path) {
        PdBase.addToSearchPath(path);
        this.thisCallbackContext.success("success");
    }

    /*
        This is not implemented yet, on iOS this function is required to be called from javascript. In here when it is called it doesn't do anything
     */
    public void closePatch() {
        //PdBase.closePatch();
        this.thisCallbackContext.success("success");
    }

    public void sendBang(String bangName) {
        try {
            PdBase.sendBang(bangName);
            this.thisCallbackContext.success("success");
        } catch (Exception e) {
            this.thisCallbackContext.error("Error: " + e.toString());
        }
    }

    public void sendFloat(String flag, String floatVal) {
        try {
            Float finalFloatVal = Float.parseFloat(floatVal);
            PdBase.sendFloat(flag, finalFloatVal);
            this.thisCallbackContext.success("success");
        } catch (Exception e) {
            this.thisCallbackContext.error("Error: " + e.toString());
        }
    }

    private boolean isHeadsetEnabled() {
        final AudioManager audioManager = (AudioManager) this.cordova.getActivity().getSystemService(Context.AUDIO_SERVICE);
        return audioManager.isWiredHeadsetOn() ||
                audioManager.isBluetoothA2dpOn() ||
                audioManager.isBluetoothScoOn();
    }

}