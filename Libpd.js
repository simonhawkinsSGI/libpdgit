/*
Developed by: Sahil Alaracu - Serious Games International
Date: 15th June 2015
*/

/* The two arguments should be functions callbacks of type "void(void)"
   cbOk: fired if the initialization of libPd succeded
   cbKo (optional): fired if the initialization fails

   Note: the init function is asynchronous, so the calling code shouldn't be
   window.plugins.libPd.init(....)
   window.plugins.libPd.openPatch(....)
   This could mess things up, as the initialization does take a while.
   Instead, cbOk should be the function responsible for opening the patch, as it's
   fired upon the completion of the initialization process.

   Example of usage

	---- Initialise the plugin in OnDeviceReady function

    Libpd.init(function (msg) {}, function (err) {});

    ---- Then to open the patch do folllowing: 

     Libpd.openPatch('patch_zip_file.zip', function (msg) {
            if (msg == 'success') {
                utils.removeLoader(); //remove loading icon
            }
        }, function (err) {
            utils.removeLoader();
            if (err == "HEADPHONES_NOT_PLUGGED_IN") {
                alert("Please plugin your headphones");
            }
        });

*/



cordova.define("com.sgil.libpd.Libpd",
function(require, exports, module) {

var exec = require('cordova/exec');


function Libpd() {
    this.resultCallback = null; // Function
}

Libpd.prototype.init = function(successCallback, failureCallback) {
     exec(successCallback,failureCallback, "Libpd", "init", []);
};

Libpd.prototype.deinit = function(successCallback, failureCallback) {
   exec(successCallback,failureCallback, "Libpd", "deinit", []);
};

Libpd.prototype.openPatch = function (patchName, successCallback, failureCallback) {
    console.log("Libpd.js: turnonpd");
    exec(successCallback,failureCallback, "Libpd", "openPatch", patchName);
};

Libpd.prototype.closePatch = function(successCallback, failureCallback) {
      exec(successCallback,failureCallback, "Libpd", "closePatch", []);
};

Libpd.prototype.addPath = function(path, successCallback,failureCallback) {
  var args = {};
  args.path = path;
  exec(successCallback,failureCallback, "Libpd", "addPath", [args]);
};

Libpd.prototype.sendBang = function(receiver, successCallback,failureCallback) {
  var args = {};
  args.receiver = receiver;
  exec(successCallback,failureCallback, "Libpd", "sendBang", [args]);
};

Libpd.prototype.sendFloat = function(num, receiver, successCallback,failureCallback) {
  var args = {};
  args.receiver = receiver;
  args.num = num;
  exec(successCallback,failureCallback, "Libpd", "sendFloat", [args]);

};

module.exports = new Libpd();
});